package ru.nsu.fit.endpoint.manager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.*;

class CustomerManagerTest {
    private IDBService dbService;
    private CustomerManager customerManager;

    private CustomerPojo createCustomerInput;

    @BeforeEach
    void init() {
        // create stubs for the test's class
        dbService = mock(IDBService.class);
        Logger logger = mock(Logger.class);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    void testCreateCustomer_firstNameHasCorrectFormat() {
        createCustomerInput = createCustomer();

        CustomerPojo createCustomerOutput = createCustomer();
        createCustomerOutput.id = UUID.randomUUID();

        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);
        when(dbService.getCustomerIdByLogin(anyString())).thenThrow(new IllegalArgumentException("err"));

        List<String> correctFirstNames = Arrays.asList(
                "Jh",                                           // Строка длиной 2
                "Jhordan",                                      // Строка длиной 7
                "Jhordanianin"                                  // Строка длиной 12
        );

        for (String correctFirstName : correctFirstNames) {
            createCustomerInput.firstName = correctFirstName;
            createCustomerOutput.firstName = correctFirstName;

            CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

            assertEquals(customer.firstName, createCustomerOutput.firstName);
        }

        verify(dbService, times(correctFirstNames.size())).getCustomerIdByLogin(anyString());
        verify(dbService, times(correctFirstNames.size())).createCustomer(createCustomerInput);
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_lastNameHasCorrectFormat() {
        createCustomerInput = createCustomer();

        CustomerPojo createCustomerOutput = createCustomer();
        createCustomerOutput.id = UUID.randomUUID();

        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);
        when(dbService.getCustomerIdByLogin(anyString())).thenThrow(new IllegalArgumentException("err"));

        List<String> correctLastNames = Arrays.asList(
                "Wi",                                           // Строка длиной 2
                "Wickers",                                      // Строка длиной 7
                "Wickllelonga"                                  // Строка длиной 12
        );

        for (String correctLastName : correctLastNames) {
            createCustomerInput.lastName = correctLastName;
            createCustomerOutput.lastName = correctLastName;

            CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

            assertEquals(customer.lastName, createCustomerOutput.lastName);
        }

        verify(dbService, times(correctLastNames.size())).getCustomerIdByLogin(anyString());
        verify(dbService, times(correctLastNames.size())).createCustomer(createCustomerInput);
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_loginHasCorrectFormat() {
        createCustomerInput = createCustomer();

        CustomerPojo createCustomerOutput = createCustomer();
        createCustomerOutput.id = UUID.randomUUID();

        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);
        when(dbService.getCustomerIdByLogin(anyString())).thenThrow(new IllegalArgumentException("err"));

        List<String> correctLogins = Arrays.asList(
                "jhon_wick@gmail.com",
                "jhon_wick@[192.168.2.1]",
                "jhon-wick@g-mail.com",
                "jhon%wick@gmail.com",
                "jhon!wick@gmail.com",
                "j@gmail.com"
        );

        for (String correctLogin : correctLogins) {
            createCustomerInput.login = correctLogin;
            createCustomerOutput.login = correctLogin;

            CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

            assertEquals(customer.login, createCustomerOutput.login);
        }

        verify(dbService, times(correctLogins.size())).getCustomerIdByLogin(anyString());
        verify(dbService, times(correctLogins.size())).createCustomer(createCustomerInput);
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_passHasCorrectFormat() {
        createCustomerInput = createCustomer();

        CustomerPojo createCustomerOutput = createCustomer();
        createCustomerOutput.id = UUID.randomUUID();

        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);
        when(dbService.getCustomerIdByLogin(anyString())).thenThrow(new IllegalArgumentException("err"));

        List<String> correctPasses = Arrays.asList(
                "baba_y",
                "baba_yaga",
                "baba_yaga!!1",
                "baba_jhon",
                "baba_wik"
        );

        for (String correctPass : correctPasses) {
            createCustomerInput.pass = correctPass;
            createCustomerOutput.pass = correctPass;

            CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

            assertEquals(customer.pass, createCustomerOutput.pass);
        }

        verify(dbService, times(correctPasses.size())).getCustomerIdByLogin(anyString());
        verify(dbService, times(correctPasses.size())).createCustomer(createCustomerInput);
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_balanceIsZero() {
        createCustomerInput = createCustomer();
        createCustomerInput.balance = 0;

        CustomerPojo createCustomerOutput = createCustomer();
        createCustomerOutput.id = UUID.randomUUID();
        createCustomerOutput.balance = 0;

        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);
        when(dbService.getCustomerIdByLogin(anyString())).thenThrow(new IllegalArgumentException("err"));

        CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

        assertEquals(customer.balance, createCustomerOutput.balance);

        verify(dbService, times(1)).getCustomerIdByLogin(anyString());
        verify(dbService, times(1)).createCustomer(createCustomerInput);
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_customerDataIsNull() {
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(null));
        assertEquals(CustomerManager.NULL_CUSTOMER_DATA_ERROR, exception.getMessage());

        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_firstNameIsNull() {
        createCustomerInput = createCustomer();
        createCustomerInput.firstName = null;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals(CustomerManager.FIRST_NAME_IS_NULL_ERROR, exception.getMessage());

        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }


    @Test
    void testCreateCustomer_firstNameHasWrongFormat() {
        createCustomerInput = createCustomer();

        List<String> wrongFirstNames = Arrays.asList(
                "",                                             // пустая строка
                "     ",                                        // строка из пробелов
                "Jh on",                                        // строка с пробелом в середине
                "jhon",                                         // строка, начинающася со строчной буквы
                "JhoN",                                         // строка с непервой заглавной буквой
                "Jhon1985",                                     // строка с цифрами
                "J",                                            // строка длиной 1
                "Johnjhonjhonh",                                // строка длиной 13
                "Wolfeschlegelsteinhausenbergerdorff ",         // длинная строка
                "Jh()n$ "                                       // строка со специальными символами
        );

        for (String wrongFirstName : wrongFirstNames) {
            createCustomerInput.firstName = wrongFirstName;
            Exception exception = assertThrows(IllegalArgumentException.class, () ->
                    customerManager.createCustomer(createCustomerInput));
            assertEquals(CustomerManager.FIRST_NAME_ERROR, exception.getMessage());
        }
        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_lastNameIsNull() {
        createCustomerInput = createCustomer();
        createCustomerInput.lastName = null;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals(CustomerManager.LAST_NAME_IS_NULL_ERROR, exception.getMessage());

        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_lastNameHasWrongFormat() {
        createCustomerInput = createCustomer();

        List<String> wrongLastNames = Arrays.asList(
                "",                                             // пустая строка
                "     ",                                        // строка из пробелов
                "Wi ck",                                        // строка с пробелом в середине
                "wick",                                         // строка, начинающася со строчной буквы
                "WicK",                                         // строка с непервой заглавной буквой
                "Wick1985",                                     // строка с цифрами
                "W",                                            // строка длиной 1
                "WickWickWickk",                                // строка длиной 13
                "Wolfeschlegelsteinhausenbergerdorff ",         // длинная строка
                "W|ck$ "                                       // строка со специальными символами
        );

        for (String wrongLastName : wrongLastNames) {
            createCustomerInput.lastName = wrongLastName;
            Exception exception = assertThrows(IllegalArgumentException.class, () ->
                    customerManager.createCustomer(createCustomerInput));
            assertEquals(CustomerManager.LAST_NAME_ERROR, exception.getMessage());
        }
        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_loginIsNull() {

        createCustomerInput = createCustomer();
        createCustomerInput.login = null;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals(CustomerManager.LOGIN_IS_NULL_ERROR, exception.getMessage());

        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_loginHasWrongFormat() {
        createCustomerInput = createCustomer();

        List<String> wrongLogins = Arrays.asList(
                "",                                             // пустая строка
                "     ",                                        // строка из пробелов
                "john_wick",                                    // только локальная часть
                "gmail.ru",                                     // только домен
                ".jhon_wick@gmail.ru",                          // c первой точкой в локальной части
                "jhon_wick.@gmail.ru",                          // c последней точкой в локальной части
                "jhon wick@gmail ru",                           // c пробелами
                "jhon_wick.gmail.ru",                           // без @
                "jhon@wick@gmail.ru",                           // с несколькими @
                ";jh()_[wi,ck].gmail.ru",                       // co спецальными символами в локальной части
                "jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick" +
                        "_jhon_wick_jhon_wick.gmail.ru"        // c длинной локальной частью
        );

        for (String wrongLogin : wrongLogins) {
            createCustomerInput.login = wrongLogin;
            Exception exception = assertThrows(IllegalArgumentException.class, () ->
                    customerManager.createCustomer(createCustomerInput));
            assertEquals(CustomerManager.LOGIN_ERROR, exception.getMessage());
        }

        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_customerExists() {
        createCustomerInput = createCustomer();

        when(dbService.getCustomerIdByLogin(anyString())).thenReturn(UUID.randomUUID());

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals(String.format(CustomerManager.CUSTOMER_EXISTS_ERROR, createCustomerInput.login), exception.getMessage());
    }

    @Test
    void testCreateCustomer_passIsNull() {
        createCustomerInput = createCustomer();
        createCustomerInput.pass = null;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                customerManager.createCustomer(createCustomerInput));
        assertEquals(CustomerManager.PASS_IS_NULL_ERROR, exception.getMessage());

        verify(dbService, only()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_passHasWrongFormat() {
        createCustomerInput = createCustomer();

        List<String> wrongPasses = Arrays.asList(
                "ba",                                             // строка длиной 2
                "baba_",                                          // строка длиной 5
                "baba_yaga!!!1",                                  // строка длиной 13
                "baba_yaga_forevor!!1"                            // строка длиной 20
        );

        for (String wrongPass : wrongPasses) {
            createCustomerInput.pass = wrongPass;
            Exception exception = assertThrows(IllegalArgumentException.class, () ->
                    customerManager.createCustomer(createCustomerInput));
            assertEquals(CustomerManager.PASS_ERROR, exception.getMessage());
        }

        verify(dbService, times(wrongPasses.size())).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_passIsEasy() {
        createCustomerInput = createCustomer();

        List<String> easyPass = Arrays.asList(
                "123qwe",
                "123Qwe",
                "1q2w3e",
                "1q2W3e",
                "john_t_great",
                "joHn_t_great",
                "awesome_wick",
                "awesome_WicK",
                "jhonwick",
                "JHONWICK"
        );

        for (String wrongPass : easyPass) {
            createCustomerInput.pass = wrongPass;
            Exception exception = assertThrows(IllegalArgumentException.class, () ->
                    customerManager.createCustomer(createCustomerInput));
            assertEquals(CustomerManager.PASS_IS_EASY, exception.getMessage());
        }

        verify(dbService, times(easyPass.size())).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreateCustomer_balanceIsNotZero() {
        createCustomerInput = createCustomer();

        List<Integer> wrongBalances = Arrays.asList(
                -1,
                1,
                1000
        );

        for (Integer wrongBalance : wrongBalances) {
            createCustomerInput.balance = wrongBalance;

            Exception exception = assertThrows(IllegalArgumentException.class, () ->
                    customerManager.createCustomer(createCustomerInput));
            assertEquals(CustomerManager.BALANCE_ERROR, exception.getMessage());
        }

        verify(dbService, times(wrongBalances.size())).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    private static CustomerPojo createCustomer() {
        CustomerPojo customerPojo = new CustomerPojo();
        customerPojo.firstName = "John";
        customerPojo.lastName = "Wick";
        customerPojo.login = "john_wick@gmail.com";
        customerPojo.pass = "Baba_Jaga";
        customerPojo.balance = 0;
        return customerPojo;
    }
}