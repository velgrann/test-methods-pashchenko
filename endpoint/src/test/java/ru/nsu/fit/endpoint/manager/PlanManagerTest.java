package ru.nsu.fit.endpoint.manager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;
import ru.nsu.fit.endpoint.database.data.PlanPojo;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class PlanManagerTest {
    private IDBService dbService;
    private PlanManager planManager;

    private PlanPojo createPlanInput;

    @BeforeEach
    void init() {
        // create stubs for the test's class
        dbService = mock(IDBService.class);
        Logger logger = mock(Logger.class);

        // create the test's class
       planManager = new PlanManager(dbService, logger);
    }

    @Test
    void testCreatePlan_nameHasCorrectFormat() {
        createPlanInput = createPlan();

        PlanPojo createPlanOutput = createPlan();
        createPlanOutput.id = UUID.randomUUID();

        when(dbService.createPlan(createPlanInput)).thenReturn(createPlanOutput);

        String name128lenth = "a";
        for (int i=0; i<127; i++) name128lenth =name128lenth+"b";

        List<String> correctNames = Arrays.asList(
                "p2",                                           // Строка длиной 2
                "plan2",                                      // Строка длиной 5
                name128lenth                                 // Строка длиной 128
        );

        for (String correctName : correctNames) {
            createPlanInput.name = correctName;
            createPlanOutput.name = correctName;

            PlanPojo plan = planManager.createPlan (createPlanInput);

            assertEquals(plan.name, createPlanOutput.name);
        }

        verify(dbService, times(correctNames.size())).createPlan(createPlanInput);
        verify(dbService, never()).getCustomerIdByLogin(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createCustomer(anyObject());
    }

    @Test
    void testCreatePlan_detailsHasCorrectFormat() {
        createPlanInput = createPlan();

        PlanPojo createPlanOutput = createPlan();
        createPlanOutput.id = UUID.randomUUID();

        when(dbService.createPlan(createPlanInput)).thenReturn(createPlanOutput);

        String details1024lenth = "a";
        for (int i=0; i<1023; i++) details1024lenth =details1024lenth+"b";

        List<String> correctDetails = Arrays.asList(
                "n.",                                           // Строка длиной 2
                "plan2",                                     // Строка длиной 5
                details1024lenth                                // Строка длиной 1024
        );

        for (String correctDetail : correctDetails) {
            createPlanInput.details = correctDetail;
            createPlanOutput.details = correctDetail;

            PlanPojo plan = planManager.createPlan (createPlanInput);

            assertEquals(plan.details, createPlanOutput.details);
        }

        verify(dbService, times(correctDetails.size())).createPlan(createPlanInput);
        verify(dbService, never()).getCustomerIdByLogin(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createCustomer(anyObject());
    }

    @Test
    void testCreatePlan_feeHasCorrectFormat() {
        createPlanInput = createPlan();

        PlanPojo createPlanOutput = createPlan();
        createPlanOutput.id = UUID.randomUUID();

        when(dbService.createPlan(createPlanInput)).thenReturn(createPlanOutput);

        int[] correctFees = {
                0,
                6544,
                999999
        };

        for (int correctFee : correctFees) {
            createPlanInput.fee = correctFee;
            createPlanOutput.fee = correctFee;

            PlanPojo plan = planManager.createPlan (createPlanInput);

            assertEquals(plan.details, createPlanOutput.details);
        }

        verify(dbService, times(correctFees.length)).createPlan(createPlanInput);
        verify(dbService, never()).getCustomerIdByLogin(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createCustomer(anyObject());
    }

    @Test
    void testCreatePlan_nameIsNull() {
        createPlanInput = createPlan();
        createPlanInput.name = null;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                planManager.createPlan(createPlanInput));
        assertEquals(PlanManager.NAME_IS_NULL_ERROR, exception.getMessage());

        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreatePlan_detailsIsNull() {
        createPlanInput = createPlan();
        createPlanInput.details = null;

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                planManager.createPlan(createPlanInput));
        assertEquals(PlanManager.DETAILS_IS_NULL_ERROR, exception.getMessage());

        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreatePlan_nameHasWrongFormat() {
        createPlanInput = createPlan();

        String name129lenth = "a";
        for (int i=0; i<128; i++) name129lenth =name129lenth+"b";

        List<String> wrongNames = Arrays.asList(
                "",                                             // пустая строка
                "     ",                                        // строка из пробелов
                name129lenth,                                   // строка длиной 129
                "h",                                            // строка длиной 1
                "!nv!s "                                       // строка со специальными символами
        );

        for (String wrongName : wrongNames) {
            createPlanInput.name = wrongName;
            Exception exception = assertThrows(IllegalArgumentException.class, () ->
                    planManager.createPlan(createPlanInput));
            assertEquals(PlanManager.NAME_ERROR, exception.getMessage());
        }
        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreatePlan_detailsHasWrongFormat() {
        createPlanInput = createPlan();

        String details1205lenth = "a";
        for (int i=0; i<1024; i++) details1205lenth = details1205lenth+"b";

        List<String> wrongDetails = Arrays.asList(
                "",                                             // пустая строка
                details1205lenth                               // строка длиной 1025
        );

        for (String wrongDetail : wrongDetails) {
            createPlanInput.details = wrongDetail;
            Exception exception = assertThrows(IllegalArgumentException.class, () ->
                    planManager.createPlan(createPlanInput));
            assertEquals(PlanManager.DETAILS_ERROR, exception.getMessage());
        }
        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    @Test
    void testCreatePlan_feeHasWrongFormat() {
        createPlanInput = createPlan();

        int[] wrongFees = {
                -1,
                1000000
        };

        for (int wrongFee : wrongFees) {
            createPlanInput.fee = wrongFee;
            Exception exception = assertThrows(IllegalArgumentException.class, () ->
                    planManager.createPlan(createPlanInput));
            assertEquals(PlanManager.FEE_ERROR, exception.getMessage());
        }
        verify(dbService, never()).getCustomerIdByLogin(anyString());
        verify(dbService, never()).createCustomer(anyObject());
        verify(dbService, never()).getCustomers();
        verify(dbService, never()).createPlan(anyObject());
    }

    /*@Test
    void testCreatePlan_nameExists() {
        createPlanInput = createPlan();

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                planManager.createPlan(createPlanInput));
        assertEquals(String.format(PlanManager.PLAN_EXISTS_ERROR, createPlanInput.name), exception.getMessage());
    }*/


    private static PlanPojo createPlan() {
        PlanPojo planPojo= new PlanPojo();
        planPojo.name = "plan2";
        planPojo.details= "Some_details_for_you";
        planPojo.fee = 55;
        return planPojo;
    }
}
