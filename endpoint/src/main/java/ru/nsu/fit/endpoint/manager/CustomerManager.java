package ru.nsu.fit.endpoint.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.CustomerPojo;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;

public class CustomerManager extends ParentManager {
    private static final String IS_NULL_ERROR_MESSAGE = "%s is null";
    private static final String HAS_WRONG_FORMAT_ERROR_MESSAGE = "%s has wrong format: %s";

    static final String NULL_CUSTOMER_DATA_ERROR = String.format(IS_NULL_ERROR_MESSAGE, "CustomerData");

    private static final Pattern NAME_PATTERN = Pattern.compile("^[A-Z][a-z]{1,11}$");

    static final String FIRST_NAME_IS_NULL_ERROR = String.format(IS_NULL_ERROR_MESSAGE, "FirstName");
    static final String FIRST_NAME_ERROR = String.format(HAS_WRONG_FORMAT_ERROR_MESSAGE, "FirstName", NAME_PATTERN.pattern());

    static final String LAST_NAME_IS_NULL_ERROR = String.format(IS_NULL_ERROR_MESSAGE, "LastName");
    static final String LAST_NAME_ERROR = String.format(HAS_WRONG_FORMAT_ERROR_MESSAGE, "LastName", NAME_PATTERN.pattern());


    private static final Pattern EMAIL_PATTERN = Pattern.compile("" +
            "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|" +
            "\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|" +
            "\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]" +
            "*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}" +
            "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:" +
            "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|" +
            "\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");

    static final String LOGIN_IS_NULL_ERROR = String.format(IS_NULL_ERROR_MESSAGE, "Login");
    static final String LOGIN_ERROR = String.format(HAS_WRONG_FORMAT_ERROR_MESSAGE, "Login", EMAIL_PATTERN.pattern());
    static final String CUSTOMER_EXISTS_ERROR = "Customer with login %s already exists";

    private static final Pattern PASS_PATTERN = Pattern.compile("^.{6,12}$");

    static final String PASS_IS_NULL_ERROR = String.format(IS_NULL_ERROR_MESSAGE, "Pass");
    static final String PASS_ERROR = String.format(HAS_WRONG_FORMAT_ERROR_MESSAGE, "Pass", PASS_PATTERN.pattern());
    static final String PASS_IS_EASY = "Password is too easy.";

    static final String BALANCE_ERROR = "Balance should be 0";

    public CustomerManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * balance - должно быть равно 0.
     */
    public CustomerPojo createCustomer(CustomerPojo customer) {

        // customerData не null
        Validate.notNull(customer, NULL_CUSTOMER_DATA_ERROR);

        // firstName не null
        Validate.isTrue(Objects.nonNull(customer.firstName), FIRST_NAME_IS_NULL_ERROR);

        // firstName валидно
        Validate.isTrue(NAME_PATTERN.matcher(customer.firstName).matches(), FIRST_NAME_ERROR);

        // lastName не null
        Validate.isTrue(Objects.nonNull(customer.lastName), LAST_NAME_IS_NULL_ERROR);

        // lastName валидно
        Validate.isTrue(NAME_PATTERN.matcher(customer.lastName).matches(), LAST_NAME_ERROR);

        // login не null
        Validate.isTrue(Objects.nonNull(customer.login), LOGIN_IS_NULL_ERROR);

        // login валидно
        Validate.isTrue(EMAIL_PATTERN.matcher(customer.login).matches(), LOGIN_ERROR);

        // отсутствует customer c таким же login
        UUID existsId = null;
        try {
            existsId = dbService.getCustomerIdByLogin(customer.login);
        } catch (Exception e) {
            //ignore
        }

        Validate.isTrue(Objects.isNull(existsId), String.format(CUSTOMER_EXISTS_ERROR, customer.login));

        // pass не null
        Validate.isTrue(Objects.nonNull(customer.pass), PASS_IS_NULL_ERROR);

        // pass валидно
        Validate.isTrue(PASS_PATTERN.matcher(customer.pass).matches(), PASS_ERROR);

        Validate.isTrue(!customer.pass.equalsIgnoreCase("123qwe") &&
                !customer.pass.equalsIgnoreCase("1q2w3e") &&
                !customer.pass.toLowerCase().contains(customer.login.toLowerCase()) &&
                !customer.pass.toLowerCase().contains(customer.firstName.toLowerCase()) &&
                !customer.pass.toLowerCase().contains(customer.lastName.toLowerCase()), PASS_IS_EASY);

        // balance равен 0
        Validate.isTrue(customer.balance == 0, BALANCE_ERROR);

        return dbService.createCustomer(customer);
    }

    /**
     * Метод возвращает список объектов типа customer.
     */
    public List<CustomerPojo> getCustomers() {
        return dbService.getCustomers();
    }

    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public CustomerPojo updateCustomer(CustomerPojo customer) {
        throw new NotImplementedException("Please implement the method.");
    }

    public void removeCustomer(UUID id) {
        throw new NotImplementedException("Please implement the method.");
    }

    /**
     * Метод добавляет к текущему баласу amount.
     * amount - должен быть строго больше нуля.
     */
    public CustomerPojo topUpBalance(UUID customerId, int amount) {
        throw new NotImplementedException("Please implement the method.");
    }
}
