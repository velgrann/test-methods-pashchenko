package ru.nsu.fit.endpoint.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.database.IDBService;
import ru.nsu.fit.endpoint.database.data.PlanPojo;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;

public class PlanManager extends ParentManager {

    private static final String IS_NULL_ERROR_MESSAGE = "%s is null";

    private static final String HAS_WRONG_FORMAT_ERROR_MESSAGE = "%s has wrong format: %s";



    private static final Pattern NAME_PATTERN = Pattern.compile("^[A-Za-z0-9]{2,128}$");

    static final String NAME_IS_NULL_ERROR = String.format(IS_NULL_ERROR_MESSAGE, "Name");

    static final String NAME_ERROR = String.format(HAS_WRONG_FORMAT_ERROR_MESSAGE, "Name", NAME_PATTERN.pattern());

    static final String PLAN_EXISTS_ERROR = "Plan with %s name is already exists";



    private static final Pattern DETAILS_PATTERN = Pattern.compile("^.{1,1024}$");

    static final String DETAILS_IS_NULL_ERROR = String.format(IS_NULL_ERROR_MESSAGE, "details");

    static final String DETAILS_ERROR = String.format(HAS_WRONG_FORMAT_ERROR_MESSAGE, "details", DETAILS_PATTERN.pattern());



    static final String FEE_ERROR = "Fee should be from 0 to 999999";


    public PlanManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Plan. Ограничения:
     * name - длина не больше 128 символов и не меньше 2 включительно не содержит спец символов. Имена не пересекаются друг с другом;
    /* details - длина не больше 1024 символов и не меньше 1 включительно;
    /* fee - больше либо равно 0 но меньше либо равно 999999.
     */
    public PlanPojo createPlan(PlanPojo plan) {

        // name не null
        Validate.isTrue(Objects.nonNull(plan.name), NAME_IS_NULL_ERROR);

        // name валидно
        Validate.isTrue(NAME_PATTERN.matcher(plan.name).matches(), NAME_ERROR);

        //details не null
        Validate.isTrue(Objects.nonNull(plan.details), DETAILS_IS_NULL_ERROR);

        // details валидно
        Validate.isTrue(DETAILS_PATTERN.matcher(plan.details).matches(), DETAILS_ERROR);

        // fee валидно
        Validate.isTrue(plan.fee >= 0 && plan.fee<=999999, FEE_ERROR);




        return dbService.createPlan(plan);
    }

    public PlanPojo updatePlan(PlanPojo plan) {
        throw new NotImplementedException("Please implement the method.");
    }

    public void removePlan(UUID id) {
        throw new NotImplementedException("Please implement the method.");
    }

    /**
     * Метод возвращает список планов доступных для покупки.
     */
    public List<PlanPojo> getPlans(UUID customerId) {
        throw new NotImplementedException("Please implement the method.");
    }
}
